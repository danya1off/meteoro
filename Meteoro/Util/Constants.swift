//
//  Constants.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Constants {
    
    private init() {}
    
    static let date: String = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Date())
    }()
    
    
    static let appToken = "GRXVyjr14dfyzskdLLqwPa25Q"
    static let appTokenHeader = "X-App-Token"
    static let googleMapsAPI = "AIzaSyCuN8c2stUGiONvnf_YorbLhVhEqXSV9W0"
    static let endpointURL = "https://data.nasa.gov/resource/y77d-th95.json?$where=year%20between%20%272011-01-01%27%20and%20%27\(date)%27"
    static let lastUpdatedDate = "lastFetchedDate"
    static let meteoriteCell = "meteoriteCell"
    
    static let helveticaMedium = "HelveticaNeue-Medium"
    static let helveticaRegular = "HelveticaNeue"
    static let helveticaBold = "HelveticaNeue-Bold"
    static let helveticaCondensed = "HelveticaNeue-CondensedBold"
    
}
