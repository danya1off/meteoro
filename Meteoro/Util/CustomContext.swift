//
//  CustomContext.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class CustomContext {
    
    // Define context as singletone
//    static let shared = CustomContext()
    
    static var getContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    private init() {}
    
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Meteoro")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
