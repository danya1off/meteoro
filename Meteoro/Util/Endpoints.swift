//
//  Endpoints.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Endpoints {
    
    private init() {}
    
    static func getData(completion: @escaping (AppResult<Data>) -> Void) {
        
        guard let url = URL(string: Constants.endpointURL) else {
            return completion(.error("Endpoint URL is invalid!"))
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(Constants.appToken, forHTTPHeaderField: Constants.appTokenHeader)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                return completion(.error((error?.localizedDescription)!))
            }
            
            guard let data = data else {
                return completion(.error("Error while loading data!"))
            }
            completion(.success(data))
        }.resume()
        
    }
    
}
