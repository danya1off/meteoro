//
//  UIViewController+Ext.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/9/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func alertError(withTitle title: String, andMessage message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
