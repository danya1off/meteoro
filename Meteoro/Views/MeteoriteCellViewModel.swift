//
//  MeteoriteCellViewModel.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/9/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class MeteoriteCellViewModel {
    
    var id: Int
    var name: String
    var fall: String
    var mass: Double
    var nameType: String
    var recClass: String
    var year: Date
    
    required init(meteorite: Meteorite) {
        
        id = meteorite.id
        name = meteorite.name
        fall = meteorite.fall
        mass = meteorite.mass
        nameType = meteorite.nameType
        recClass = meteorite.recClass
        year = meteorite.year
        
    }
    
}
