//
//  MeteorsViewModel.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class MeteorsViewModel {
    
    var meteorites = [Meteorite]()
    var meteoritesCellsArray = [MeteoriteCellViewModel]()
    
    // Get data from rest api or from CoreData
    // Rest API will be call once a day and saved in CoreData for offline working
    func getData(completion: @escaping (AppResult<[Meteorite]>) -> Void) {

        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        let currentDate = dateFormatter.string(from: date)
        let userDefaulst = UserDefaults.standard
        let lastUpdatedDate = userDefaulst.string(forKey: Constants.lastUpdatedDate)
        
        // Check is there data in DB or empty. Generally need for check is the app launched at first time
        let res = getDataFromDB()
        switch res {
        case .success(let meteorites):
            
            // check if the data in CoreData is empty or is there new day started for refresh data from server
            if currentDate != lastUpdatedDate || meteorites.isEmpty {
                
                // save current date for checking
                userDefaulst.set(currentDate, forKey: Constants.lastUpdatedDate)
                
                // clear CoreData for persisting new data
                clearData()
                
                // get data from rest endpoint (NASA)
                Endpoints.getData { result in
                    switch result {
                    case .success(let data):
                        self.parse(data: data) { result in
                            switch result {
                            case .success(let data):
                                completion(.success(data))
                            case .error(let error):
                                completion(.error(error))
                            }
                        }
                    case .error(let error):
                        completion(.error(error))
                    }
                }
            } else {
                
                // if there are data in CoreData and the new day not begins, just fetche data from CoreData and send to UI
                populateCellView(withMeteors: meteorites)
                completion(.success(meteorites))
            }
        case .error(let error):
            completion(.error(error))
        }
        
    }
    
    // Parsing json data using Swift 4 new features (Decodable & Codable)
    private func parse(data d: Data, completion: @escaping (AppResult<[Meteorite]>) -> Void) {
        
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        do {
            meteorites = try decoder.decode([Meteorite].self, from: d)
            DispatchQueue.global(qos: .background).async {
                self.save(data: self.meteorites)
            }
            
            // sorting meteorites by mass (descenging)
            let converted = meteorites.sorted(by: {
                $0.mass > $1.mass
            })
            meteorites = converted
            // append data to viewModel for Cells
            self.populateCellView(withMeteors: converted)
            
            completion(.success(converted))
        } catch let error {
            completion(.error(error.localizedDescription))
        }
        
    }
    
    // save all data in CoreData
    private func save(data meteorites: [Meteorite]) {
        
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = CustomContext.getContext.persistentStoreCoordinator
        
        managedObjectContext.perform {
            for meteoro in meteorites {
                let meteoroEntity = MeteoriteEntity(context: managedObjectContext)
                meteoroEntity.id = Int64(meteoro.id)
                meteoroEntity.name = meteoro.name
                meteoroEntity.fall = meteoro.fall
                meteoroEntity.mass = meteoro.mass
                meteoroEntity.nameType = meteoro.nameType
                meteoroEntity.recClass = meteoro.recClass
                meteoroEntity.latitude = meteoro.latitude
                meteoroEntity.longitude = meteoro.longitude
                meteoroEntity.year = meteoro.year as NSDate
                
                let geolocationEntity = GeolocationEntity(context: managedObjectContext)
                geolocationEntity.type = meteoro.geolocation.type
                geolocationEntity.coordinates = meteoro.geolocation.coordinates
                meteoroEntity.geolocation = geolocationEntity
            }
            
            do {
                try managedObjectContext.save()
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
    }
    
    // fetched all data from CoreData ordering by mass (descending)
    private func getDataFromDB() -> AppResult<[Meteorite]> {
        let fetchReq: NSFetchRequest<MeteoriteEntity> = MeteoriteEntity.fetchRequest()
        let sortdescriptor = NSSortDescriptor(key: "mass", ascending: false)
        let descriptors = [sortdescriptor]
        fetchReq.sortDescriptors = descriptors
        do {
            let meteoriteEntities = try CustomContext.getContext.fetch(fetchReq)
            
            // converting from Entity object to Model Object
            meteorites = MeteoriteEntity.convertToModel(data: meteoriteEntities)
            return .success(meteorites)
        } catch let errorMsg {
            return .error(errorMsg.localizedDescription)
        }
    }
    
    // clear all data from CoreData for persist fresh one
    private func clearData() {
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: MeteoriteEntity.fetchRequest())
        do {
            try CustomContext.getContext.execute(deleteRequest)
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    // return objects count in meteorites array
    func numberOfMeteorites() -> Int {
        return meteorites.count
    }
    
    // return viewModel for cells
    func cellViewModel(index: Int) -> MeteoriteCellViewModel {
        return meteoritesCellsArray[index]
    }
    
    // get single meteorite
    func getMeteorite(atIndex index: Int) -> Meteorite {
        return meteorites[index]
    }
    
    // appending all objects to viewModel array for cells
    private func populateCellView(withMeteors meteorites: [Meteorite]) {
        meteorites.forEach {
            self.meteoritesCellsArray.append(MeteoriteCellViewModel(meteorite: $0))
        }
    }
    
}

