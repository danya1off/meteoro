//
//  MeteorsTVCTableViewController.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class MeteorsTVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    
    let tableView = UITableView()
    private(set) var viewModel: MeteorsViewModel!
    private var filteredMeteorites = [Meteorite]()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MeteorsViewModel()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search meteorites by name"
        definesPresentationContext = true
        
        setupTableView()
        populateData()
    }

    private func populateData() {
        viewModel.getData { [unowned self] result in
            switch result {
            case .success(_):
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .error(let error):
                DispatchQueue.main.async {
                    self.alertError(withTitle: "Error!", andMessage: error)
                }
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredMeteorites = viewModel.meteorites.filter{ meteorite -> Bool in
            return meteorite.name.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }

}

extension MeteorsTVC {
    
    // create custom navigation bar
    private func setupNavigationBar() {
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.gray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 6.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.7
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
        
        navigationItem.title = "Meteoro"
    
    }
    
    // setup tableView
    func setupTableView() {
        
        setupNavigationBar()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorInset.left = 25
        tableView.separatorInset.right = 25
        tableView.tableHeaderView = searchController.searchBar
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(MeteoriteCell.self, forCellReuseIdentifier: Constants.meteoriteCell)
        
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredMeteorites.count
        }
        return viewModel.numberOfMeteorites()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.meteoriteCell, for: indexPath) as! MeteoriteCell
        if isFiltering() {
            cell.cellViewModel = MeteoriteCellViewModel(meteorite: filteredMeteorites[indexPath.row])
        } else {
            cell.cellViewModel = viewModel.cellViewModel(index: indexPath.row)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mapVC = MapVC()
        var selectedMeteorite: Meteorite!
        if isFiltering() {
            selectedMeteorite = filteredMeteorites[indexPath.row]
        } else {
            selectedMeteorite = viewModel.getMeteorite(atIndex: indexPath.row)
        }
        mapVC.meteorite = selectedMeteorite
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(mapVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
