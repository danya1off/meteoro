//
//  MapVC.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/9/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import GoogleMaps

class MapVC: UIViewController {
    
    var mapView: GMSMapView!
    var meteorite: Meteorite!

    override func viewDidLoad() {
        super.viewDidLoad()
        createUI()
        setCoordinates()
    }
    
    // setting coordinates
    func setCoordinates() {
        let mapCamera = GMSCameraPosition.camera(withLatitude: meteorite.latitude, longitude: meteorite.longitude, zoom: 6.0)
        mapView.camera = mapCamera
        
        let marker = GMSMarker()
        marker.position = mapCamera.target
        marker.map = mapView
    }

}

extension MapVC: GMSMapViewDelegate {
    
    // create custom UI
    func createUI() {
        
        navigationItem.title = meteorite.name
        
        mapView = GMSMapView()
        mapView.delegate = self
        mapView.settings.zoomGestures = true
        mapView.settings.myLocationButton = true
        mapView.settings.scrollGestures = true
        mapView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            
            mapView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
            
            ])
        
    }
    
    // override mapView func to customize the information window under the pin
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let infoView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 70))
        
        infoView.backgroundColor = .white
        infoView.layer.cornerRadius = 5
        
        let meteoriteNameLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = UIColor.darkGray
            label.textAlignment = .center
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.text = meteorite.name
            return label
        }()
        infoView.addSubview(meteoriteNameLbl)
        
        let massLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = UIColor.gray
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 12)
            label.text = "Mass: \(meteorite.mass)"
            return label
        }()
        infoView.addSubview(massLbl)
        
        let classLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = UIColor.gray
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 12)
            label.text = "Class: \(meteorite.recClass)"
            return label
        }()
        infoView.addSubview(classLbl)
        
        NSLayoutConstraint.activate([
            
            meteoriteNameLbl.topAnchor.constraint(equalTo: infoView.topAnchor, constant: 10),
            meteoriteNameLbl.leftAnchor.constraint(equalTo: infoView.leftAnchor, constant: 5),
            meteoriteNameLbl.rightAnchor.constraint(equalTo: infoView.rightAnchor, constant: -5),
            
            massLbl.topAnchor.constraint(equalTo: meteoriteNameLbl.bottomAnchor, constant: 3),
            massLbl.leadingAnchor.constraint(equalTo: meteoriteNameLbl.leadingAnchor),
            massLbl.trailingAnchor.constraint(equalTo: meteoriteNameLbl.trailingAnchor),
            
            classLbl.topAnchor.constraint(equalTo: massLbl.bottomAnchor, constant: 3),
            classLbl.leadingAnchor.constraint(equalTo: meteoriteNameLbl.leadingAnchor),
            classLbl.trailingAnchor.constraint(equalTo: meteoriteNameLbl.trailingAnchor)
            
            ])
        
        return infoView
        
    }
    
}
