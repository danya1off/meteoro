//
//  MeteoriteCell.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/9/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class MeteoriteCell: UITableViewCell {
    
    private var nameLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.gray
        label.font = UIFont(name: Constants.helveticaBold, size: 14)
        return label
    }()
    
    private var classLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: Constants.helveticaRegular, size: 12)
        return label
    }()
    
    private var dateLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: Constants.helveticaMedium, size: 12)
        return label
    }()
    
    private var massLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: Constants.helveticaRegular, size: 12)
        return label
    }()
    
    private var fallLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.lightGray
        label.font = UIFont(name: Constants.helveticaRegular, size: 12)
        return label
    }()

    weak var cellViewModel: MeteoriteCellViewModel! {
        didSet {
            
            nameLbl.text = cellViewModel.name
            classLbl.text = "Class: \(cellViewModel.recClass)"
            dateLbl.text = getFormatted(date: cellViewModel.year)
            massLbl.text = "Mass: \(cellViewModel.mass)"
            fallLbl.text = "Fall: \(cellViewModel.fall)"
            
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getFormatted(date d: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: d)
    }
    
    private func setupCell() {
        
        backgroundColor = UIColor(red:0.94, green:0.95, blue:0.94, alpha:1.0)
        
        addSubview(nameLbl)
        addSubview(classLbl)
        addSubview(massLbl)
        addSubview(dateLbl)
        addSubview(fallLbl)
        
        nameLbl.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        nameLbl.leftAnchor.constraint(equalTo: leftAnchor, constant: 25).isActive = true
        
        classLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor, constant: 5).isActive = true
        classLbl.leadingAnchor.constraint(equalTo: nameLbl.leadingAnchor).isActive = true
        
        massLbl.topAnchor.constraint(equalTo: classLbl.topAnchor).isActive = true
        massLbl.leftAnchor.constraint(equalTo: classLbl.rightAnchor, constant: 15).isActive = true
        
        dateLbl.topAnchor.constraint(equalTo: nameLbl.topAnchor).isActive = true
        dateLbl.rightAnchor.constraint(equalTo: rightAnchor, constant: -25).isActive = true
        
        fallLbl.topAnchor.constraint(equalTo: massLbl.topAnchor).isActive = true
        fallLbl.trailingAnchor.constraint(equalTo: dateLbl.trailingAnchor).isActive = true
        
        
    }
    
}
