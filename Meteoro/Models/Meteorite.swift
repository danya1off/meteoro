//
//  Meteorite.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/8/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Meteorite: Codable {
    
    var id: Int
    var name: String
    var fall: String
    var mass: Double
    var nameType: String
    var recClass: String
    var latitude: Double
    var longitude: Double
    var year: Date
    var geolocation: Geolocation
    
    enum CodingKeys: String, CodingKey {
        case id, name, fall, mass, year, geolocation
        case nameType = "nametype"
        case recClass = "recclass"
        case latitude = "reclat"
        case longitude = "reclong"
    }
    
}

extension Meteorite {
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        guard let newId = try Int(values.decode(String.self, forKey: .id)) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.mass], debugDescription: "Can't convert String to Int in decode phase!"))
        }
        id = newId
        
        name = try values.decode(String.self, forKey: .name)
        fall = try values.decode(String.self, forKey: .fall)
        nameType = try values.decode(String.self, forKey: .nameType)
        recClass = try values.decode(String.self, forKey: .recClass)
        guard let newLatitude = try Double(values.decode(String.self, forKey: .latitude)) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.mass], debugDescription: "Can't convert String to Double in decode phase!"))
        }
        latitude = newLatitude
        guard let newLongitude = try Double(values.decode(String.self, forKey: .longitude)) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.mass], debugDescription: "Can't convert String to Double in decode phase!"))
        }
        longitude = newLongitude
        year = try values.decode(Date.self, forKey: .year)
        geolocation = try values.decode(Geolocation.self, forKey: .geolocation)
        guard let newMass = try Double(values.decode(String.self, forKey: .mass)) else {
            throw DecodingError.dataCorrupted(.init(codingPath: [CodingKeys.mass], debugDescription: "Can't convert String to Double in decode phase!"))
        }
        mass = newMass
    }
    
}
