//
//  Geolocation.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/9/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Geolocation: Codable {
    var type: String
    var coordinates: [Double]
}
