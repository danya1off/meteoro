//
//  GeolocationEntity+CoreDataProperties.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/10/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension GeolocationEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GeolocationEntity> {
        return NSFetchRequest<GeolocationEntity>(entityName: "GeolocationEntity")
    }

    @NSManaged public var coordinates: [Double]?
    @NSManaged public var type: String?
    @NSManaged public var meteorite: MeteoriteEntity?

}
