//
//  MeteoriteEntity+CoreDataProperties.swift
//  Meteoro
//
//  Created by Jeyhun Danyalov on 12/10/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension MeteoriteEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MeteoriteEntity> {
        return NSFetchRequest<MeteoriteEntity>(entityName: "MeteoriteEntity")
    }

    @NSManaged public var fall: String?
    @NSManaged public var id: Int64
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var mass: Double
    @NSManaged public var name: String?
    @NSManaged public var nameType: String?
    @NSManaged public var recClass: String?
    @NSManaged public var year: NSDate?
    @NSManaged public var geolocation: GeolocationEntity?

    static func convertToModel(data: [MeteoriteEntity]) -> [Meteorite] {
        
        let meteorites = data.map { meteorite in
            return Meteorite(id: Int(meteorite.id),
                             name: meteorite.name!,
                             fall: meteorite.fall!,
                             mass: meteorite.mass,
                             nameType: meteorite.nameType!,
                             recClass: meteorite.recClass!,
                             latitude: meteorite.latitude,
                             longitude: meteorite.longitude,
                             year: meteorite.year! as Date,
                             geolocation: Geolocation(type: meteorite.geolocation!.type!,
                                                      coordinates: meteorite.geolocation!.coordinates!))
        }
        return meteorites
    }
    
}
