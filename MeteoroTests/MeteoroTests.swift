//
//  MeteoroTests.swift
//  MeteoroTests
//
//  Created by Jeyhun Danyalov on 12/10/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import XCTest
@testable import Meteoro

class MeteoroTests: XCTestCase {
    
    var viewModel: MeteorsViewModel!
    
    override func setUp() {
        super.setUp()
        
        viewModel = MeteorsViewModel()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testApiCall() {
        
        var meteorites = [Meteorite]()
        var errorMsg = ""
        
        viewModel.getData { result in
            switch result {
            case .success(let data):
                meteorites = data
            case .error(let error):
                errorMsg = error
            }
        }
        
        XCTAssert(meteorites.isEmpty)
        XCTAssert(errorMsg.isEmpty)
        
    }
    
}
